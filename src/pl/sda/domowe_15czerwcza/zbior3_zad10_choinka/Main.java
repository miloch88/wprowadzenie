package pl.sda.domowe_15czerwcza.zbior3_zad10_choinka;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę warstw choinki: ");
        int n = scanner.nextInt();
        String gwiazdka = "*";

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n * 2; j++) {
                if ((j < n - i) || j > (n + i)) {
                    System.out.print(" ");
                } else {
                    System.out.print(gwiazdka);
                }
            }
            System.out.println();
        }
    }
}



