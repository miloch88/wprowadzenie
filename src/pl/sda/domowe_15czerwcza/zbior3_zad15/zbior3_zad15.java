package pl.sda.domowe_15czerwcza.zbior3_zad15;

import java.util.Scanner;

public class zbior3_zad15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj słowo: ");
        String slowo = scanner.nextLine();

        int h = slowo.length();
        int k= (h-1);
        boolean pal = true;

        for (int i = 0; i <h ; i++) {
            if(slowo.charAt(i) != slowo.charAt(k)){
                pal = false;
            }
            k--;
        }
        if(pal){
            System.out.println("Wyraz jest palindromem.");
        }else{
            System.out.println("Wyraz nie jest palindromem.");
        }
    }
}
