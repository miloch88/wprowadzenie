package pl.sda.domowe_15czerwcza.zbior3_zad9;

import java.util.Random;
import java.util.Scanner;

public class zbior3_zad9 {
    public static void main(String[] args){

        Scanner scanner  = new Scanner(System.in);

        Random generator = new Random();
        int wygenerowana = generator.nextInt(99)+1;

        int liczbaOdUzytkownika;
        do{
            System.out.println("Zgadnij liczbę: ");
            liczbaOdUzytkownika = scanner.nextInt();
            if(liczbaOdUzytkownika>wygenerowana){
                System.out.println("Liczba jest mniejsza. Zgaduj dalej.");
            }else if (liczbaOdUzytkownika<wygenerowana){
                System.out.println("Liczba jest większa. Zgaduj dalej.");
            }
        }while(liczbaOdUzytkownika != wygenerowana);

        System.out.println("Gratuluję, zgdaywana liczba to: " + wygenerowana);


    }
}
