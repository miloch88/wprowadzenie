package pl.sda.domowe_15czerwcza.zbior3_zad14;

import java.util.Scanner;

public class zbior3_zad14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        boolean[][] tablica = new boolean[n][n];

//        tablica[0][0]= Boolean.parseBoolean(" ");

        for (int i = 0; i < n; i++) {
            for( int j=0; j< n; j++){
                if(i<=j){
                    if(i%j == 0)tablica[i][j]=true;
                    else tablica[i][j]=false;
                }else{
                    if(j%i==0)tablica[i][j]=true;
                    else tablica[i][j]=false;
                }
            }

        }

        //Wyświetlanie tablicy

        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++)
                System.out.print(tablica[i][j]+" ");
                System.out.println();


        }

    }

}
