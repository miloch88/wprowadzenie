package pl.sda.domowe_15czerwcza.zbior3_zad4;

import java.sql.SQLOutput;
import java.util.Scanner;

public class zbior3_zad4 {
    public static void main(String[] arga){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz początek zakresu: ");
        int poczatekZakresu = scanner.nextInt();

        System.out.println("Wpisz koniec zakresu: ");
        int koniecZakresu = scanner.nextInt();

        if(poczatekZakresu<koniecZakresu){
            System.out.println("Wpisz dzielnik: ");
            int dzielnik = scanner.nextInt();
            System.out.print("Wszystkie liczby podzielne przez dzielnik z podanego zakresu: ");
            for (int i = poczatekZakresu; i < koniecZakresu; i++) {
                if(i%dzielnik == 0){
                    System.out.print(i + " ");
                }
            }

        }else{
            System.out.println("Koniec zakresu jest mniejszy od początku.");
        }
    }
}
