package pl.sda.domowe_15czerwcza.zbior3_zad11;

import java.util.Scanner;

public class zbior3_zad11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę ");
        int n = scanner.nextInt();

        System.out.print("Dzielniki Twojej liczby to: ");
        for (int i = 1; i <= n; i++) {
            if(n%i == 0){
                System.out.print(i+ " ");
            }

        }
    }
}
