package pl.sda.domowe_15czerwcza.zbior3_zad6;

import java.util.Scanner;

public class zbior3_zad6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Wpisz liczbę większą od zera: ");
        int n = scanner.nextInt();
        int a = 1;

        do {
            if (a < n) {
                System.out.println(a);
                a *=2;
            }
        } while (a < n);

    }
}
