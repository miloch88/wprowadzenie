package pl.sda.zajecia_19czerwca.obiektowosc.kontoBankowe;

public class Main {
    public static void main(String[] args) {

        KontoBankowe kontoAndrzeja = new KontoBankowe(11, 1000);
        KontoBankowe kontoBeaty = new KontoBankowe(100, 1000);

        System.out.println("Przed przelewem:");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();

//        int pobranaKwota = kontoBeaty.pobracSrodki(100);
//        kontoAndrzeja.wplacSrodki(pobranaKwota);

        kontoAndrzeja.wplacSrodki(kontoBeaty.pobracSrodki(100));

        System.out.println("Po przelweie:");
        kontoAndrzeja.wyswietlStanKonta();
        kontoBeaty.wyswietlStanKonta();

        KontoBankowe kontoCwaniaczka = new KontoBankowe(123,0);
        kontoCwaniaczka.wyswietlStanKonta();

        System.out.println("Czary Mary");
//        kontoCwaniaczka.stanKonta =100000;
        kontoCwaniaczka.wyswietlStanKonta();


    }
}
