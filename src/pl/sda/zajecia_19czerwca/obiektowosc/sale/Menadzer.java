package pl.sda.zajecia_19czerwca.obiektowosc.sale;

public class Menadzer {
    String imie;
    Sala[] sale;

    void wyswietlDostepneSale() {
        System.out.println("Dostępne sale:");
        for (Sala sala : sale) {
            if (sala.czyJestWolna) {
                sala.wyswietlOpisSali();
            }
        }

    }

    boolean zabookujSale(String nazwaSali) {
        for (Sala sala : sale) {
            if (sala.nazwa.equals(nazwaSali) && sala.czyJestWolna) {
                sala.czyJestWolna = false;
                return true;
            }
        }
        return false;
    }
}
