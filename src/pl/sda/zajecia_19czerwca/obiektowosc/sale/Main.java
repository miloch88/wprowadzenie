package pl.sda.zajecia_19czerwca.obiektowosc.sale;

public class Main {
    public static void main(String[] args) {
        Sala gdansk = new Sala();
        gdansk.nazwa = "Gdańsk";
        gdansk.iloscM2 = 50.05;
        gdansk.liczbaStanowsik = 8;
        gdansk.czyJestRzutnik = true;

        Sala sztykaW = new Sala();
        sztykaW.nazwa = "Java";
        sztykaW.iloscM2 = 45;
        sztykaW.liczbaStanowsik = 12;
        sztykaW.czyJestRzutnik = false;

        Sala nowa = new Sala("Nowa", 100.02, 30, false);

        Sala[] zarzadaneSale = new Sala[]{gdansk, sztykaW, nowa};

        Menadzer jan = new Menadzer();
        jan.imie = "Jan";
        jan.sale = zarzadaneSale;
        jan.wyswietlDostepneSale();

        System.out.println();

        jan.zabookujSale("Java");
        jan.wyswietlDostepneSale();
    }
}
