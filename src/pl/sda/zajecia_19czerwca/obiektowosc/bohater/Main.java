package pl.sda.zajecia_19czerwca.obiektowosc.bohater;

public class Main {
    public static void main(String[] args) {

        SuperBohater bohater1 = new SuperBohater("Spiderman", "Wyrzucanie sieci");
        SuperBohater bohater2 = new SuperBohater("Batman", "Wyrzucanie nietoperzy");
        SuperBohater bohater3 = new SuperBohater("Jarek K.", "Kłócenie się ze wszystkimi");

        //a:
        bohater1=bohater2; bohater1=null;
        System.out.println(bohater2==null);

        //b:
        bohater1=null; bohater2=bohater1; bohater1=bohater3;
        System.out.println(bohater1==null);
        System.out.println(bohater2==null);
        System.out.println(bohater3==null);

//        //5. sprawdz equals i ==
//        System.out.println(bohater1.equals(bohater2);
//        System.out.println(bohater1==null);
//        System.out.println(bohater3==null);

    }
}
