package pl.sda.zajecia_19czerwca.obiektowosc.psiaki;

public class Pies {
    String imie;
    int rok;
    String kolor;

    public static void main(String[] args) {
        Pies burek = new Pies();
        burek.imie = "Burek";
        burek.rok = 2015;
        burek.kolor = "Brązowy";

        System.out.println("Burek");
        burek.dajGlos();

        Pies corgi = new Pies();
        corgi.imie = "Mietek";
        corgi.rok = 2016;
        corgi.kolor = "rudawy";

        System.out.println("Mietek");
        corgi.dajGlos();


    }

    private void dajGlos() {
        int wiek = 2018 - rok;
        for (int i = 0; i < wiek; i++) {
            System.out.println("Woof!");
        }
    }
}
