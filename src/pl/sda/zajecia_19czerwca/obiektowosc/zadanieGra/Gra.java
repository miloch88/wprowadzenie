package pl.sda.zajecia_19czerwca.obiektowosc.zadanieGra;

import java.util.Random;

public class Gra {
    int zgadywanaLiczba;
    int liczbaZgadniec;
    boolean czyKoniecGry;
    boolean czyZwiestwo;

    public Gra() {
        Random random = new Random();
        zgadywanaLiczba = random.nextInt(9) + 1;
    }

    public boolean czyToTenNumer(int numer) {
        liczbaZgadniec++;
        if (liczbaZgadniec > 5) {
            czyKoniecGry = true;
        }
        if (numer < zgadywanaLiczba) {
            System.out.println("Liczba jest większa");
            return false;
        } else if (numer > zgadywanaLiczba) {
            System.out.println("Podana liczba jest mniejsza");
            return false;
        } else {
            System.out.println("Zgadłeś");
            czyZwiestwo = true;
            czyKoniecGry = true;
            return true;
        }

    }

    public boolean czyKoniec() {
        if (liczbaZgadniec >= 5 || czyKoniecGry) {
            return true;
        }
        return false;
    }
}
