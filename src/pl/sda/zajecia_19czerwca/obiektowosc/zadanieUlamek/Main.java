package pl.sda.zajecia_19czerwca.obiektowosc.zadanieUlamek;

public class Main {
    public static void main(String[] args) {

        Ulamek pol = new Ulamek(1,2);

        pol.wyswietlUlamek();

        Ulamek cwierc = pol.pomnoz(pol);
        cwierc.wyswietlUlamek();

        Ulamek osiem = pol.pomnoz(pol).pomnoz(pol);
        osiem.wyswietlUlamek();

        Ulamek ulamek2 = new Ulamek(2,3);

        Ulamek ulamek4 = ulamek2.podziel(pol);
        ulamek4.wyswietlUlamek();

        Ulamek ulamek5 = ulamek4.dodaj(pol);
        ulamek5.wyswietlUlamek();

        Ulamek ulamek6= ulamek5.odejmij(ulamek4);
        ulamek6.wyswietlUlamek();

    }
}
