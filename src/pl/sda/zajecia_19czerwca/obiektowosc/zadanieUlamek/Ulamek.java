package pl.sda.zajecia_19czerwca.obiektowosc.zadanieUlamek;

public class Ulamek {
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public void wyswietlUlamek() {

        skorcenieUlamka();

        // Wyciąga całości z ułamków.
        if (licznik % mianownik == 0) {
            System.out.println(licznik / mianownik);
        } else if (licznik % mianownik != 0 && licznik > mianownik) {
            System.out.println(String.format("%d i %d/%d", (licznik / mianownik), (licznik % mianownik), mianownik));
        } else {
            System.out.println(String.format("%d/%d", licznik, mianownik));
        }
    }

    public Ulamek pomnoz(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik;
        int mianownik = this.mianownik * ulamek.licznik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek dodaj(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik + ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    //Skracanie ułamków.
    public void skorcenieUlamka() {
        int nwd = 1;
        for (int i = 2; i < mianownik; i++)
            if (licznik % i == 0 && mianownik % i == 0) {
                nwd = i;
            }
        this.licznik = licznik / nwd;
        this.mianownik = mianownik / nwd;
    }

    public Ulamek odejmij(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik - ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;
        return new Ulamek(licznik, mianownik);
    }
}


