package pl.sda.zajecia_19czerwca.obiektowosc.osoby;

public class Osoba {
    String imie;
    int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public Osoba() {
    }

    public static void main(String[] args) {

        Osoba ania = new Osoba();
        ania.imie = "Ania";
        ania.wiek = 25;

        Osoba andrzej = new Osoba();
        andrzej.imie = "Andrzej";
        andrzej.wiek = 54;

        Osoba mariola = new Osoba();
        mariola.imie = "Mariola";
        mariola.wiek = 68;

        Osoba maciek = new Osoba("Maciek", 15);
        Osoba kasia = new Osoba("Kasia", 28);
        Osoba bartek = new Osoba("Bartek", 45);

        Osoba[] wszyscy = new Osoba[]{ania, andrzej, mariola, maciek, kasia, bartek};

//        for (int i = 0; i < wszyscy.length; i++) {
//            wszyscy[i].przedstawSie();
//        }
        System.out.println();

        for (Osoba i : wszyscy) {
            i.przedstawSie();
        }

        wyswietlPanie(wszyscy);

        wyswietlPanów(wszyscy);


//        ania.przedstawSie();
//        andrzej.przedstawSie();
//        mariola.przedstawSie();
    }

    private static void wyswietlPanie(Osoba[] wszyscy) {
        System.out.println();
        System.out.println("Panie to: ");
        for (Osoba i : wszyscy) {
            if (i.imie.endsWith("a"))
                i.przedstawSie();
        }
    }

    private static void wyswietlPanów(Osoba[] wszyscy) {
        System.out.println();
        System.out.println("Panowie to: ");
        for (Osoba i : wszyscy) {
            if (!i.imie.endsWith("a"))
                i.przedstawSie();
        }
    }

    private void przedstawSie() {
//        System.out.println("Mam na imię " + imie+ " i mam " + wiek + " lat.");
        System.out.printf("Mam na imię %s i mam %d lat. \n", imie, wiek);
    }

}