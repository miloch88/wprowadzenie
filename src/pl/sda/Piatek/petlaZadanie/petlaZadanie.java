package pl.sda.Piatek.petlaZadanie;

public class petlaZadanie {
    public static void main(String[] args) {
        //a
        for(int i=1; i<101; i++){
            System.out.println(i);
        }
        System.out.println();
        //b
        for(int i = 1000; i < 1021; i++){
            System.out.print(i+ ", ");
        }
        System.out.println("\n");

        //c
        for(int i = -30; i < 1000; i++){
            if (i%5 == 0){
                System.out.println(i);
            }
        }
        System.out.println();
        //d
        for(int i = 1; i < 101; i++){
            if (i%3 == 0){
                System.out.println(i);
            }
        }
        System.out.println();
        //e
        for(int i=30; i<301; i++){
            if (i%3 == 0 && i%5 == 0){
                System.out.println(i);
            }
        }
        System.out.println();
        //f
        for(int i=-300; i <301; i++){
            if(i%2 != 0){
                System.out.print(i + ";");
            }
        }
        System.out.println("\n");
        //g
        for(int i = -100; i<100; i++){
            if(i%2 == 0){
                System.out.print(i+";");
            }
        }
        System.out.println("\n");
        //h
        for(char i = 'a'; i<'z'; i++){
            System.out.println(i);
        }
        System.out.println();
        //i
        for(char i = 'A'; i<'Z'; i++){
            System.out.println(i);
        }
        System.out.println();
        //j
        for(char i = 'A'; i<'Z'; i+=2 ){
            System.out.println(i);
        }
        System.out.println();
        //k
        for(char i = 'b'; i<'z'; i+=2){
            if(i%5 == 0){
                System.out.println(i);
            }
        }
        System.out.println();
        //l
        for(int i = 1; i<101; i++){
            System.out.println(i+ ". Hello World");
        }
    }
}
