//package pl.sda.Piatek.petlaWhile;
//
//public class petlaWhile {
//    public static void main(String[] args) {
//        //Trzeba zmienić int (...)
//
//        //a
//        int a=1;
//        while (a<101) {
//            System.out.println(a);
//            a++;
//        }
//        System.out.println();
//        //b
//        int i = 1000;
//        while (i < 1021) {
//            System.out.print(i+ ", ");
//            i++;
//        }
//        System.out.println("\n");
//
//        //c
//        int i = -30;
//        while (i < 1000) {
//            if (i%5 == 0){
//                System.out.println(i);
//            }
//            i++;
//        }
//        System.out.println();
//        //d
//        int i = 1;
//        while (i < 101) {
//            if (i%3 == 0){
//                System.out.println(i);
//            }
//            i++;
//        }
//        System.out.println();
//        //e
//        int i=30;
//        while (i<301) {
//            if (i%3 == 0 && i%5 == 0){
//                System.out.println(i);
//            }
//            i++;
//        }
//        System.out.println();
//        //f
//        int i=-300;
//        while (i <301) {
//            if(i%2 != 0){
//                System.out.print(i + ";");
//            }
//            i++;
//        }
//        System.out.println("\n");
//        //g
//        int i = -100;
//        while (i<100) {
//            if(i%2 == 0){
//                System.out.print(i+";");
//            }
//            i++;
//        }
//        System.out.println("\n");
//        //h
//        char i = 'a';
//        while (i<'z') {
//            System.out.println(i);
//            i++;
//        }
//        System.out.println();
//        //i
//        char i = 'A';
//        while (i<'Z') {
//            System.out.println(i);
//            i++;
//        }
//        System.out.println();
//        //j
//        char i = 'A';
//        while (i<'Z') {
//            System.out.println(i);
//            i += 2;
//        }
//        System.out.println();
//        //k
//        char i = 'b';
//        while (i<'z') {
//            if(i%5 == 0){
//                System.out.println(i);
//            }
//            i += 2;
//        }
//        System.out.println();
//        //l
//        int i = 1;
//        while (i<101) {
//            System.out.println(i+ ". Hello World");
//            i++;
//        }
//    }
//}
