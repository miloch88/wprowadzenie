package pl.sda.zajecia_18czerwca;

import java.util.Scanner;

public class zajecia_zad4 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String tekst = scanner.nextLine();
        String slowo = "ania";

        //a) Występowanie słowa
        if (tekst.contains(slowo)) {
            System.out.println("Występuje słowo: " + slowo);
        } else System.out.println("Nie wystepuje slowo: " + slowo);

        //b) Czy zaczyna się od:
        if (tekst.startsWith(slowo)) {
            System.out.println("Zaczyna się od : " + slowo);
        } else System.out.println("Nie zaczyna się: " + slowo);

        //c) Czy kończy się na:
        if (tekst.endsWith(slowo)) {
            System.out.println("Kończy się na: " + slowo);
        } else System.out.println("Nie kończy się na: " + slowo);

        //d) Czy jest równe
        if (tekst.equals(slowo)) {
            System.out.println("Jest równe z : " + slowo);
        } else System.out.println("Nie nie jest równe z: " + slowo);

        //e) Czy zawiera słowo nie zależnie do wielkośći liter
        if (tekst.toLowerCase().contains(slowo)) {
            System.out.println("Zawiera słowo : " + slowo + " niezależnie od wielkości liter");
        } else System.out.println("Nie zawiera słowa : " + slowo + " niezależnie od wielkości liter");

        //f) Zwracanie
        System.out.println(tekst.indexOf(slowo));

    }
}
