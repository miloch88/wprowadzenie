
//1. Utwórz program ‚Kalkulator’
//        1. Nadpisz punkt wejścia, tak aby wyświetlał następującą
//        instrukcję po uruchomieniu:
//        „Podaj rodzaj działania : *,+,-,/”
//        „Podaj pierwszą liczbę”
//        „Podaj druga liczbę”
//        „Otrzymany wynik {działanie} wynosi: {wynik}”
//        2. Użyj switcha do wybrania rodzaju metody
//        3. Użyj switcha do wydrukowania końcowego wyniku
//        (np. ‚+’  ’dodawania’) np. po wpisaniu przez użytkownika
//        ‚+’, ‚2’, ‚3’ wyświetlony zostanie tekst:
//        Wynik dodawania liczb 2 i 3 wynosi: 5

package pl.sda.zajecia_18czerwca;

import java.util.Scanner;

public class zajecia_zad3 {


    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        String dzialanie = scanner.nextLine();

        System.out.println("Podaj pierwszą liczbę: ");
        int a = scanner.nextInt();
        System.out.println("Podaj drugą liczbę: ");
        int b = scanner.nextInt();


        switch (dzialanie) {
            case "*":
                System.out.println("Wynik mnożenia liczb " + a + " i " + b +" wynosi: " +mnozenie(a, b));
                break;
            case "/":
                System.out.println("Wynik dzielenia liczb " + a + " i " + b +" wynosi: "+ dzielenie(a, b));
                break;
            case "+":
                System.out.println("Wynik dodawania liczb " + a + " i " + b +" wynosi: " + dodawanie(a, b));
                break;
            case "-":
                System.out.println("Wynik odejmowania liczb " + a + " i " + b +" wynosi: " + odejmowanie(a, b));
                break;
            default:
                System.out.println("Niepoprawne działanie.");

        }
    }



    public static int mnozenie(int a, int b) {
        return a * b;
    }

    public static int dzielenie(int a, int b) {
        return a / b;
    }

    public static int dodawanie(int a, int b) {
        return a + b;
    }

    public static int odejmowanie(int a, int b) {
        return a - b;
    }

}

