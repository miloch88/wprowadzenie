package pl.sda.zajecia_18czerwca;

public class zajecia_zad1 {
    public static void main(String[] args) {

        int a = 6;
        drukujParzystoscLiczby(a);

        int b = 7;
        czyLiczbaJestParzysta(b);
        System.out.println(czyLiczbaJestParzysta(b));


    }

    public static void drukujParzystoscLiczby(int a) {

        if (a % 2 == 0) System.out.println("Liczba jest parzysta");
        else System.out.println("Liczba jest nieparzysta");

        return;

    }

    static boolean czyLiczbaJestParzysta(int b) {

        if (b % 2 == 0) return true;
        else return false;

    // Skompresowanie kodu powużej.
    // static boolean czyLiczbaJestParzysta(int b){
    //  return d % 2 == 0;

    }
}
