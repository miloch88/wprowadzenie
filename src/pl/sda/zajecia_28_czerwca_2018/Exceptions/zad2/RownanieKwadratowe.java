package pl.sda.zajecia_28_czerwca_2018.Exceptions.zad2;

public class RownanieKwadratowe {

    double a;
    double b;

    public RownanieKwadratowe(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    double c;
    double delta;

    public void obliczDelte() {
        try {
            delta = (b * b) - (4 * a * c);
            if (delta < 0) {
                throw new Exception("Delta mniejsza od zera");
            }
        } catch (Exception e) {
            System.out.println("Delta mniejsz od zera, brak pierwiastków");
        }
    }

    public double obliczX1() {
        return (-b + Math.sqrt(delta)) / (2 * a);
    }

    public double obliczX2() {
        return (-b - Math.sqrt(delta)) / (2 * a);
    }


}



