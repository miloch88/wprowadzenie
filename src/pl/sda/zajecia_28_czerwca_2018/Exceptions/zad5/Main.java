package pl.sda.zajecia_28_czerwca_2018.Exceptions.zad5;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        LocalDateTime today = LocalDateTime.now(); // W innym formacie ???

        String tekst;
        LocalDateTime dateTime = LocalDateTime.now().minusDays(1);
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Wpisz datę w przyszłości i w formacie: yyyy-MM-dd HH:mm");
            tekst = scanner.nextLine();

            try {
                // usunąć "T" przed czasem ???
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                dateTime = LocalDateTime.parse(tekst, dateTimeFormatter);

            } catch (DateTimeParseException e) {
                System.out.println("Jeszcze raz wpisz poprawną datę");
            }

        } while (today.isAfter(dateTime)); //today.equals()  Tu musi być porównanie patternu ???

        Duration duration = Duration.between(today, dateTime);

        System.out.println(duration.toMinutes() + " minut");

    }
}
