package pl.sda.zajecia_28_czerwca_2018.Exceptions.zad3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int a;
        int b;

        Scanner scanner = new Scanner(System.in);
        a = scanner.nextInt();
        b = scanner.nextInt();

       podziel(a,b);

    }

    private static void podziel(int a, int b) {
        try {
            if (b == 0)
                throw new CholeroException(b);
            System.out.println((double)a/b);
        } catch (Exception e) {
            System.out.println("Pamiętaj cholero, by nie dzielić przez zero!");
        }
    }
}
