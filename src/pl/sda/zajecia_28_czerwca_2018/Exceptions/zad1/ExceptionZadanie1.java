package pl.sda.zajecia_28_czerwca_2018.Exceptions.zad1;

import java.util.Scanner;

public class ExceptionZadanie1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        metoda_sposob1(a, b);

        // Za pomocą pętli if
//        if(b>=0){
//            System.out.println(a/b);
//        }else System.out.println("B jest ujemne, nie wykonam działania");

    }

    private static void metoda_sposob1(int a, int b) {
        try {
            if (b < 0) {
                throw new Exception("B<0 bbbbbbbbbbbbbbb");
                }
            System.out.println((double)a/b);
        } catch (Exception e) {
            // co ma się stać jeśli błąd wystąpi
            System.out.println("Wystąpił błąd, treść błędu: " + e.getMessage());
        }
    }

    private static void metoda_sposob2(int a, int b) throws Exception {
        if (b < 0) {
            throw new Exception("B < 0");
        }
    }
}



