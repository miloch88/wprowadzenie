package pl.sda.zajecia_28_czerwca_2018.Exceptions.zad4;

public class Club {
    public static void main(String[] args) {

        Person osoba1 = new Person("Kamil", "Kamilowski", 15);
        Person osoba2 = new Person("Basia", "Basiowska", 25);

        enter(osoba1);
        enter(osoba2);

    }


    private static void enter(Person person) {
        try {
            if (person.wiek < 18)
                throw new NoAdultException();
        System.out.println(person.imie + ", spoko loko, możesz wejść");
        } catch (NoAdultException e) {
            System.out.println(person.imie + " jesteś za młoda/ młody, masz tylko: " + person.wiek + " lat.");
        }
    }
}
