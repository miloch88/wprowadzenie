package pl.sda.zajecia_28_czerwca_2018.DateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

public class Czas {
    public static void main(String[] args) {
        String wybor;
        do {
            Scanner scanner = new Scanner(System.in);
        wybor = scanner.nextLine();

            switch (wybor) {
                case "date":
                    LocalDate date1 = LocalDate.now();
                    System.out.println(date1);
                    break;
                case "time":
                    LocalTime date2 = LocalTime.now();
                    System.out.println(date2);
                    break;
                case "datetime":
                    LocalDateTime date3 = LocalDateTime.now();
                    System.out.println(date3);
            }

        } while (!wybor.equals("quit"));
    }
}
