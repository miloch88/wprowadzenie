package pl.sda.zajecia_28_czerwca_2018.DateTime.zad6;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map <String, String> zoneIds = new HashMap<>();
        zoneIds.put("Warszawa", "Europe/Warsaw");
        zoneIds.put("Paryż", "Europe/Paris");
        zoneIds.put("Whitehorse", "Etc/UTC");
        zoneIds.put("Moskwa", "Europe/Moscow");
        zoneIds.put("Canberra", "Australia/Sydney");
        zoneIds.put("Sosnowiec", "Europe/Warsaw");


        String timezone = zoneIds.get("Canberra");
        ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of(timezone));

        System.out.println(dateTime);
    }
}
