package pl.sda.zajecia_28_czerwca_2018.DateTime;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class CzasTrzeci {
    public static void main(String[] args) {

   //     LocalDate dateX = LocalDate.of(2017,05,25);


        LocalDate date = LocalDate.now();
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


        LocalDate dataPodana = LocalDate.parse(text, dateTimeFormatter);

        Period period = Period.between(date,dataPodana);


        System.out.println("Lat: " + period.getYears() + " dni: " + period.getMonths());
    }
}
