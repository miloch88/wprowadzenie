package pl.sda.zajecia_28_czerwca_2018.DateTime;

import java.time.LocalDate;

public class CzasDrugi {
    public static void main(String[] args) {

        LocalDate date = LocalDate.now();
        System.out.println(date);

        LocalDate date1 = date.plusDays(10);
        System.out.println(date1);

        LocalDate date2 = date.minusDays(10);
        System.out.println(date2);




    }
}
