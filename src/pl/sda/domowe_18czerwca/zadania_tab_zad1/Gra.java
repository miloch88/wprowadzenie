package pl.sda.domowe_18czerwca.zadania_tab_zad1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Gra {
    public static void main(String[] args) {
        int[] typowaneLiczby = new int[6];

        typujLiczbyUzytkownika(typowaneLiczby);
        System.out.println(Arrays.toString(typowaneLiczby));

        int[] wylosowaneLiczby = new int[6];
        losujLiczby(wylosowaneLiczby);
        System.out.println("Wylosowane liczby to :\n"
                + Arrays.toString(wylosowaneLiczby));

        int wynik = obliczTrafienia(typowaneLiczby,
                wylosowaneLiczby);

        System.out.println(String.format(
                "Trafiłeś %d z %d liczb", wynik, 6
        ));
    }

    private static int obliczTrafienia(int[] typowaneLiczby, int[] wylosowaneLiczby) {
        int liczbaTrafien = 0;

        for (int i = 0; i < wylosowaneLiczby.length; i++) {
            if (trafiona(typowaneLiczby, wylosowaneLiczby[i])) {
                liczbaTrafien++;
            }
        }

        return liczbaTrafien;
    }

    private static boolean trafiona(int[] typowaneLiczby,int wylosowanaLiczba) {
        for (int typowanaLiczba : typowaneLiczby) {
            if (typowanaLiczba == wylosowanaLiczba)
                return true;
        }
        return false;
    }

    private static void losujLiczby(int[] liczby) {
        Random random = new Random();
        int wylosowanaLiczba; // wylosowana liczba
        int wylosowaneLiczby = 0; // indeks
        while (wylosowaneLiczby < 6) {
            wylosowanaLiczba = random.nextInt(48) + 1;
            if (czyUnikatowa(liczby, wylosowanaLiczba)){
                liczby[wylosowaneLiczby]=wylosowanaLiczba;
                wylosowaneLiczby++;
            }
        }
    }

    private static void typujLiczbyUzytkownika(int[] typowaneLiczby) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj 6 liczb");

        int wytypowaneLiczby = 0;//indeks
        int typowanaLiczba;//pobrana od użytkownika
        do {
            System.out.println("Podaj liczbę #" + (wytypowaneLiczby + 1));
            typowanaLiczba = scanner.nextInt();
            if (czyUnikatowa(typowaneLiczby, typowanaLiczba) && typowanaLiczba > 0 &&
                    typowanaLiczba < 50) {
                typowaneLiczby[wytypowaneLiczby] = typowanaLiczba;
                wytypowaneLiczby++;
            }
        } while (wytypowaneLiczby < 6);

    }

    private static boolean czyUnikatowa(int[] typowaneLiczby, int typowanaLiczba) {
        return !trafiona(typowaneLiczby, typowanaLiczba);
    }


}