package pl.sda.domowe_18czerwca;

import java.util.Arrays;

public class zadania_tab_zad2 {
    public static void main(String[] args) {
        int[] tablica = new int[]{5, 3, 8, 7, 4, 9};

        int min = tablica[0], max = tablica[0], sum = 0;
        double mediana;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] < min) min = tablica[i];
            if (tablica[i] > max) max = tablica[i];
            sum += tablica[i];
        }
        System.out.println("Minimum wynosi: " + min);
        System.out.println("Maksimum wynosi: " + max);
        System.out.println("Suma wynosi: " + sum);
        System.out.println("Wartość średnia wynosi: " + sum / (double) tablica.length);

        //Mediana

        Arrays.sort(tablica);
        System.out.print("Posortowana tablica to: ");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }

        System.out.println();

        //Sortowanie

        if (tablica.length % 2 == 0)
            mediana = ((double) tablica[tablica.length / 2] + (double) tablica[tablica.length / 2 - 1]) / 2;
        else
            mediana = ((double) tablica[tablica.length / 2]);

        System.out.println("Mediana wynosi: " + mediana);


    }
}
