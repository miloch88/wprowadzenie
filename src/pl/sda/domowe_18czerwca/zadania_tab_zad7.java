package pl.sda.domowe_18czerwca;

import java.util.Scanner;

import static java.lang.Character.*;

public class zadania_tab_zad7 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String zdanie = scanner.nextLine();

        int liczba = 0, spacja = 0, litera = 0, inne = 0;

        for (int i = 0; i < zdanie.length(); i++) {
            if (isDigit(zdanie.charAt(i))) {
                liczba += 1;
            }
            if (isSpaceChar(zdanie.charAt(i))) {
                spacja += 1;
            }
            if (isLetter(zdanie.charAt(i))) {
                litera += 1;
            }
            if (isDefined(zdanie.charAt(i)) && !isLetter(zdanie.charAt(i))&&!isDigit(zdanie.charAt(i))&& !isSpaceChar(zdanie.charAt(i))){
                inne += 1;

            }

        }

        System.out.printf("Podany tekst: %s zawiera: \n %d liter,\n %d spacji,\n %d liczb,\n %d inncyh znaków;", zdanie, litera, spacja, liczba, inne);
//        System.out.println(liczba);
//        System.out.println(spacja);
//        System.out.println(litera);
//        System.out.println(inne);
    }
}

