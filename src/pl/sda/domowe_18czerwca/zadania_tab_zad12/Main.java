package pl.sda.domowe_18czerwca.zadania_tab_zad12;

import java.util.Scanner;

public class Main {
    static int n = 0;

    public static void main(String[] args) {

        podajliczbe();
    }

    private static void podajliczbe() {
        Scanner scanner = new Scanner(System.in);
        do {
            n = scanner.nextInt();
            if (n > 0 && n < 21) {
                rysuj();
                break;
            } else {
                System.out.println("Podaj liczbę z przedziału od 1 do 20");
            }
        } while (n > 0 || n <25);
    }

    private static void rysuj() {
        for (int i = 0; i < n; i++) {
            for (int j = 1; j <= i+1; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}