package pl.sda.domowe_18czerwca.zadania_tab_zad15;

import java.util.Scanner;

public class Main {

    static int n, m;

    public static void main(String[] args) {

        podajLiczby();
        najwiekszyWspolny();

    }

    private static void najwiekszyWspolny() {
        int nwd;
        if (n > m) {
            nwd = m;
            for (int i = 1; i <= m; i++) {
                if (n % i == 0 && m % i == 0) {
                    nwd = i;
                }

            }

        } else {
            nwd = n;
            for (int i = 1; i <= n; i++) {
                if (n % i == 0 && m % i == 0) {
                    nwd = i;
                }

            }
        }
        System.out.printf("\nTwój największy wspólny dzielnik to %d.", nwd);
    }

    private static void podajLiczby() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby: ");
        m = scanner.nextInt();
        n = scanner.nextInt();

        System.out.printf("Twoje wybrane liczby to: %d i %d.", m, n);

    }
}


