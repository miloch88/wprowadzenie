package pl.sda.domowe_18czerwca;

import java.util.Arrays;
import java.util.Scanner;

public class zadania_tab_zad8 {

    public static void main(String[] args) {

        int[] wybraneLiczby = new int[1];
        wybraneLiczby =typowaneLiczby(wybraneLiczby);
        System.out.println();

//        W tym miejscu tabliva wybraneLiczby dalej ma indeks 5...
//        for(int i:wybraneLiczby) System.out.print(i);

        System.out.println();
        sortujOdNajmniejszej(wybraneLiczby);

        System.out.println();
        sortujOnNajwiekszej(wybraneLiczby);

    }

    private static void sortujOnNajwiekszej(int[] wybraneLiczby) {
        Arrays.sort(wybraneLiczby);
        System.out.print("Posortowana tablica od największej: ");
        for (int i = wybraneLiczby.length - 1; i > 0; i--) System.out.print(wybraneLiczby[i] + " ");


    }

    // Żeby nie było, że idę na łatwiznę. Sorotwanie pęcherzykowe
    private static void sortujOdNajmniejszej(int[] wybraneLiczby) {
        int t;

        for (int i = 1; i < wybraneLiczby.length; i++) {
            for (int j = wybraneLiczby.length - 1; j >= i; j--) {
                if (wybraneLiczby[j - 1] > wybraneLiczby[j]) {
                    //Zamienia elementy
                    t = wybraneLiczby[j - 1];
                    wybraneLiczby[j - 1] = wybraneLiczby[j];
                    wybraneLiczby[j] = t;
                }
            }
        }
//        Arrays.sort(wybraneLiczby);
        System.out.print("Posortowana tablica od najnmiejszej: ");
        for (int i = 1; i < wybraneLiczby.length; i++) System.out.print(wybraneLiczby[i] + " ");

    }

    static int[] typowaneLiczby(int[] wybraneLiczby) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz swoje liczby: ");
        int index = 0, n;
        do {
            //Zczytywanie liczb
            n = scanner.nextInt();
            //zabezpieczenie przed wykorczniem poza tablicę
            if (wybraneLiczby.length <= index) {
                //rozszerzamy tablicę
                int[] noweWybraneLiczby = new int[wybraneLiczby.length + 1];
                //przypisujemy elementy i kopiujemy starą tablicę
                for (int i = 0; i < wybraneLiczby.length; i++) {
                    noweWybraneLiczby[i] = wybraneLiczby[i];
                }
                //przypisanie nowego elemtnu
                noweWybraneLiczby[index] = n;
                //zwiększanie licznika
                index++;
                //zastąpienie starje tablicy nową
                wybraneLiczby = noweWybraneLiczby;

            } else {
                //jeśli tablica jest wystarczająca
                wybraneLiczby[index] = n;
                index++;

            }

        } while (n != 0);


        //Wyświetlamy naszą tablicę:
        System.out.print("Twoje wybrane liczby to: ");
        for (int i = 0; i < index - 1; i++) {
            System.out.print(wybraneLiczby[i] + " ");
        }
        return wybraneLiczby;
    }
}