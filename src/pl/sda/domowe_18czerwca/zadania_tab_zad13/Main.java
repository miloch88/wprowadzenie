package pl.sda.domowe_18czerwca.zadania_tab_zad13;

public class Main {
    static String gwiazdka = "*";

    public static void main(String[] args) {

        rysuj();
        System.out.println();
        rysuj1();
        System.out.println();
        rysuj2();
        System.out.println();
        rysuj3();

    }

    private static void rysuj3() {
        for (int i = 8; i > 0; i--) {
            for (int j = 0; j < 8; j++) {
                if (j < i) {
                    System.out.print(" ");
                } else System.out.print(gwiazdka);
            }
            System.out.println();
        }
    }

    private static void rysuj2() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (j < i) {
                    System.out.print(" ");
                } else System.out.print(gwiazdka);
            }
            System.out.println();
        }
    }

    private static void rysuj1() {
        for (int i = 0; i < 8; i++) {
            for (int j = 8; j > i; j--) {
                System.out.print(gwiazdka);
            }
            System.out.println();
        }

    }

    private static void rysuj() {

        for (int i = 0; i < 8; i++) {
            for (int j = 1; j <= i + 1; j++) {
                System.out.print(gwiazdka);
            }
            System.out.println();
        }
    }
}