package pl.sda.domowe_18czerwca;

import java.util.Scanner;

public class zadania_tab_zad3 {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj dwie liczby: ");

        //a podzielnych przez 2

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        for (int i = 0; i < n ; i++) {
            if(i%2 == 0) System.out.print(i + " ");
        }

        System.out.println();

        //b podzielnych przez 3

        for (int i = 0; i < n ; i++) {
            if(i%3 == 0) System.out.print(i + " ");
        }

        System.out.println();

        //c podzielnych przez zadaną liczbę

        for (int i = 0; i < n ; i++) {
            if(i%m == 0) System.out.print(i + " ");

        }

    }
}
