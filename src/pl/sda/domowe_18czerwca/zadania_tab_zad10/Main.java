package pl.sda.domowe_18czerwca.zadania_tab_zad10;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int index = 0;
    static int n;
    static int[] wybraneLiczby = new int[1];

    public static void main(String[] args) {

        typowaneLiczby();
        wyswietlTablice(wybraneLiczby);
        unikatoweLiczby(wybraneLiczby);
    }

    private static void unikatoweLiczby(int[] wybraneLiczby) {
        for (int i = 0; i < wybraneLiczby.length; i++) {
            for (int j = i + 1; j < wybraneLiczby.length; j++) {
                if (wybraneLiczby[i] == wybraneLiczby[j])
                    wybraneLiczby[j] = 0;
            }
        }
        System.out.println();
        wyswietlTablice(wybraneLiczby);
        System.out.println();
        System.out.println(Arrays.toString(wybraneLiczby));
    }


    private static void wyswietlTablice(int[] wybraneLiczby) {
        System.out.print("Twoje wybrane liczby to: ");
        for (int i = 0; i < index; i++) {
            System.out.print(wybraneLiczby[i] + " ");
        }
    }

     static void typowaneLiczby() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz swoje liczby: ");
        do {
            n = scanner.nextInt();
            if (n != 0) {
                if (wybraneLiczby.length <= index) {
                    int[] noweWybraneLiczby = new int[wybraneLiczby.length + 1];
                    for (int i = 0; i < wybraneLiczby.length; i++) {
                        noweWybraneLiczby[i] = wybraneLiczby[i];
                    }
                    noweWybraneLiczby[index] = n;
                    index++;
                    wybraneLiczby = noweWybraneLiczby;

                } else {
                    wybraneLiczby[index] = n;
                    index++;
                }
            }
        } while (n != 0);


    }


}

