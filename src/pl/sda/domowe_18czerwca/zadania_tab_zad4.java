package pl.sda.domowe_18czerwca;

import java.util.Scanner;

public class zadania_tab_zad4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //Liczba całkowita
        System.out.println("Wpisz liczbę całkowitą: ");
        int n = scanner.nextInt();

        //Tablica dzielników
        int liczba;
        System.out.println("Wypisz dzielniki, aby zakończyć wpisz 0: ");
        int[] dzielniki = new int[2];
        int licznikDzielniki = 0;

        do {
            // Zczytywanie dzielników
            liczba = scanner.nextInt();

            //zabezpieczenie przed wykroczeniem poza tablicę
            if (dzielniki.length <= licznikDzielniki) {
                //rozszerzamy tablicę
                int[] noweDzielniki = new int[dzielniki.length * 2];
                //przypisujemy elementy
                //kopiujemy starą tablicę do nowej
                for (int i = 0; i < dzielniki.length; i++) {
                    noweDzielniki[i] = dzielniki[i];
                }
                //przypisanie nowego elemntu
                noweDzielniki[licznikDzielniki] = liczba;

                //zwiększanie licznika
                licznikDzielniki++;
                //zastąpienie starej tablicy nową
                dzielniki = noweDzielniki;
            } else {
                //jesśli tablica jest wystarczająca
                dzielniki[licznikDzielniki] = liczba;
                licznikDzielniki++;
            }
        } while (liczba != 0);

        //Wyświetla tablicę, ale nie jest rozwiązany problem powtarzalności liczb!!!
        for (int i = 1; i < n; i++)
            for (int j = 0; j < licznikDzielniki - 1; j++) {
                if (i % dzielniki[j] == 0) {

                    System.out.print(i + " ");
                }
            }



        System.out.println();

        //Wyświetla podane zdania(co zajęło mi najwięcej czasu)
        System.out.print("Dla liczby " + n + " i dzielników: ");
        for (int i = 0; i < licznikDzielniki - 1; i++) {
            System.out.print(dzielniki[i] + ", ");
        }
        System.out.print("znleziono: \n");

        for (int i = 0; i < licznikDzielniki - 1; i++) {
            System.out.print("Liczby podzielne przez " + dzielniki[i] + ": ");
            for (int j = 1; j < n; j++) {
                if (j % dzielniki[i] == 0) System.out.print(j + " ");
            }
            System.out.println();

        }



    }
}
