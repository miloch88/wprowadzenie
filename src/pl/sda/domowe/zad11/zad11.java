package pl.sda.domowe.zad11;

import java.util.Scanner;

public class zad11 {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        double podatek = scanner.nextDouble();

        if(podatek<85528.00){
            podatek = 0.18 * podatek - 556.02;
        }else{
            podatek = 14839.02 + 0.32 * (podatek - 85528.00);
        }

        System.out.println(podatek);


    }
}
