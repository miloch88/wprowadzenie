package pl.sda.domowe.zad12;

import java.util.Scanner;

public class zad12 {
    public static void main(String[] args) {

        System.out.println("Wpisz swoją wagę: ");
        Scanner scanner1 = new Scanner(System.in);
        int waga = scanner1.nextInt();

        System.out.println("Wpisz swój wzrost: ");
        Scanner scanner2 = new Scanner(System.in);
        int wzrost = scanner2.nextInt();

        System.out.println("Wpisz swój wiek: ");
        Scanner scanner3 = new Scanner(System.in);
        int wiek = scanner3.nextInt();

        if (waga < 180 && (wzrost > 150 && wzrost < 220) && (wiek > 10 && wiek < 80)) {
            System.out.println("Spoko możesz wejść!");
        } else {
            if (waga > 180) {
                System.out.println("Jesteś za gruby");
            } else {
                if (wzrost < 150 || wzrost > 220) {
                    System.out.println("Jesteś za duży albo za mały");
                } else {
                    if(wiek>100){
                        System.out.println("To Ty jeszcze żyjesz?");
                    }else{
                        System.out.println("Jesteś za stary albo zawołaj rodziców");
                    }
                }
            }
        }
    }
}
