package pl.sda.wprowadzenie.zad7;

public class zad7 {
    public static void main(String[] args) {
        //a
        if (2 > 3) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        //b
        if (4 < 5) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        //c
        if ((2 - 2) == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        //d
        if (true) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        //e
        if (9%2 == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }
        //f
        if (9%3 == 0) {
            System.out.println(" :) ");
        } else {
            System.out.println(" :( ");
        }

    }
}
