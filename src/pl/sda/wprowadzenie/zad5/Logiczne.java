package pl.sda.wprowadzenie.zad5;

public class Logiczne {
    public static void main(String[] args) {
        System.out.println(false==false);
        System.out.println(false!=false);
        System.out.println(!true);
        System.out.println(2>4);
        System.out.println(3>5);
        System.out.println(3 == 3 && 3 == 4);
        System.out.println(3 != 5 || 3 == 5);
        System.out.println((2+4)>(1+3));
        //porównianie treści
        System.out.println("cos".equals("cos"));
        //porównanie referencji
        System.out.println("cos" == "cos");



    }
}
