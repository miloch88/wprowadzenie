package pl.sda.wprowadzenie.zad2;

import java.sql.SQLOutput;

public class PierwszyProgram {
    public static void main(String[] args) {
        String a = "Hello World!";
        String b = "Mam na imię:";
        String imie = "Michał";

        System.out.println(a);
        System.out.println(b);
        System.out.println(imie);

        System.out.println();

        String zmienna = "Hello World! ";
        System.out.println(zmienna);
        zmienna = "Mam na imię: ";
        System.out.println(zmienna);
        zmienna = "Michał";
        System.out.println(zmienna);

        System.out.println();

        System.out.println("Hello World!" + "\n" + "Mam na imię: " + "\n" + imie);

    }
}
