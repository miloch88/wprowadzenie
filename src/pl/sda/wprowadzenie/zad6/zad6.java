package pl.sda.wprowadzenie.zad6;

public class zad6 {
    public static void main(String[] args) {
        //a
        short zmiennaShort = 1;
        int zmiennaInt = zmiennaShort;
        System.out.println(zmiennaInt);
        //b
        short zmiennaShort2 = 2;
        long zmiennaLong = zmiennaShort2;
        System.out.println(zmiennaShort2);
        //c
        int zmiennaInt2 = 3;
        float zmiennaFloat = ((zmiennaInt2));
        System.out.println(zmiennaFloat);
        //d
        int zmiennaInt3 = 4;
        double zmiennnaDouble = zmiennaInt3;
        System.out.println(zmiennaInt3);
        //e
        long zmiennaLong2 = 5;
        int zmiennaInt4 = (int)zmiennaLong2;
        System.out.println(zmiennaInt4);
        //f
        short zmiennaShort3 = 6;
        byte zmiennaByte = (byte)zmiennaShort3;
        System.out.println(zmiennaByte);
        //g
        char zmiennaChar = 'A';
        int zmiennaInt5 = zmiennaChar;
        System.out.println(zmiennaInt5);

    }
}
